// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

// Екранування це заміна в тексті спеціальних символів на відповідні текстові підстановки, це потрібно для того щоб, коли нам необхідно використовувати такий символ як звичайний символ мови

// 2. Які засоби оголошення функцій ви знаєте?

// function declaration, function expression, стрілочні функції

// 3. Що таке hoisting, як він працює для змінних та функцій?

// Hoisting це можливість отримувати доступ до функцій та змінних до того, як вони були створені

// Завдання

const createUser = () => {
    const newUser = {
        firstName: prompt('Enter your first name'), 
        lastName: prompt('Enter your last name'),
        birthDay: prompt('Enter your birth date dd.mm.yyyy'),
        
        getAge: function () {
            let now = new Date();
            let currentYear = now.getFullYear();

            let inputDate = +this.birthDay.slice(0, 2);
            let inputMonth = +this.birthDay.slice(3, 5);
            let inputYear = +this.birthDay.slice(6, 10);
      
            let birthDate = new Date(inputYear, inputMonth - 1, inputDate);
            let birthYear = birthDate.getFullYear();
            let age = currentYear - birthYear;
            if (now < new Date(birthDate.setFullYear(currentYear))) {
              age = age - 1
            }
            return age;
        },
      
        getPassword() {
            return this.firstName.slice(0,1).toUpperCase() + this.lastName.toLowerCase() + this.birthDay.slice(6,10);
        },
        
        getLogin() {
            return this.firstName.slice(0,1).toLowerCase() + this.lastName.toLowerCase();
        },
    }
    
    return newUser;
    
}
const user = createUser();
console.log(user.getLogin());
console.log(user.getPassword());
console.log(user.getAge());
